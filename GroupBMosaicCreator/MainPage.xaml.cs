﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using Windows.Graphics.Imaging;
using Windows.Storage;
using Windows.Storage.Pickers;
using Windows.Storage.Streams;
using Windows.UI;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media.Imaging;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace GroupBMosaicCreator
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {

        private double dpiX;
        private double dpiY;
        private WriteableBitmap modifiedImage;
        private StorageFile sourceImageFile;

        public MainPage()
        {
            this.InitializeComponent();

            this.modifiedImage = null;
            this.dpiX = 0;
            this.dpiY = 0;

        }

        private async void AppBarButton_Click(object sender, RoutedEventArgs e)
        {
            this.sourceImageFile = await this.selectSourceImageFile();
            var copyBitmapImage = await this.makeACopyOfOriginalImageToEdit(this.sourceImageFile);

            using (var fileStream = await this.sourceImageFile.OpenAsync(FileAccessMode.Read))
            {
                var imageDecoder = await BitmapDecoder.CreateAsync(fileStream);
                this.dpiX = imageDecoder.DpiX;
                this.dpiY = imageDecoder.DpiY;

                var originalImage = copyBitmapImage;
                this.originalDisplay.Source = originalImage;

                this.mosaicDisplay.Source = copyBitmapImage;
            }
        }

        private async Task<StorageFile> selectSourceImageFile()
        {
            var openImageFilePicker = new FileOpenPicker() {
                ViewMode = PickerViewMode.Thumbnail,
                SuggestedStartLocation = PickerLocationId.PicturesLibrary
            };

            openImageFilePicker.FileTypeFilter.Add(".bmp");
            openImageFilePicker.FileTypeFilter.Add(".jpg");
            openImageFilePicker.FileTypeFilter.Add(".png");

            var imageFile = await openImageFilePicker.PickSingleFileAsync();

            if (imageFile.FileType != ".jpg" && imageFile.FileType != ".png" && imageFile.FileType != ".bmp")
            {
                var messageDialog = new MessageDialog("Invalid file type.\nPlease select a file of type .jpg, .png, or .bmp.");
                await messageDialog.ShowAsync();
            }

            return imageFile;
        }

        private async Task<BitmapImage> makeACopyOfOriginalImageToEdit(StorageFile originalImageFile)
        {
            IRandomAccessStream inputStream = await originalImageFile.OpenReadAsync();
            var newImage = new BitmapImage();
            newImage.SetSource(inputStream);

            return newImage;
        }

        private void applyBlockSize_Click(object sender, RoutedEventArgs e)
        {
            this.blockHeight.Text = this.blockWidthInput.Text;
        }

        private void AppBarButton_Click_1(object sender, RoutedEventArgs e)
        {
            this.saveWritableBitmapImage();
        }

        private async void saveWritableBitmapImage()
        {
            var saveImageFilePicker = new FileSavePicker {
                SuggestedStartLocation = PickerLocationId.PicturesLibrary,
                SuggestedFileName = "mosaicimage" + this.sourceImageFile.FileType
            };

            saveImageFilePicker.FileTypeChoices.Add("BMP files", new List<string> { ".bmp" });
            saveImageFilePicker.FileTypeChoices.Add("JPG files", new List<string> { ".jpg" });
            saveImageFilePicker.FileTypeChoices.Add("PNG files", new List<string> { ".png" });

            saveImageFilePicker.DefaultFileExtension = this.sourceImageFile.FileType;

            var saveFile = await saveImageFilePicker.PickSaveFileAsync();

            if (saveFile != null)
            {
                var stream = await saveFile.OpenAsync(FileAccessMode.ReadWrite);
                var encoder = await BitmapEncoder.CreateAsync(BitmapEncoder.PngEncoderId, stream);

                if (this.modifiedImage == null) return;

                var pixelStream = this.modifiedImage.PixelBuffer.AsStream();
                var pixels = new byte[pixelStream.Length];
                await pixelStream.ReadAsync(pixels, 0, pixels.Length);

                encoder.SetPixelData(BitmapPixelFormat.Bgra8, BitmapAlphaMode.Ignore,
                    (uint) this.modifiedImage.PixelWidth,
                    (uint) this.modifiedImage.PixelHeight, this.dpiX, this.dpiY, pixels);
                await encoder.FlushAsync();

                stream.Dispose();
            }
        }
    }
}
